class CreateStatutes < ActiveRecord::Migration[5.0]
  def change
    create_table :statutes do |t|
      t.string :ina
      t.string :usc
      t.string :title

      t.timestamps
    end
  end
end
