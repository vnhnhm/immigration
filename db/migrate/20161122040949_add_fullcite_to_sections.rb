class AddFullciteToSections < ActiveRecord::Migration[5.0]
  def change
    add_column :sections, :fullcite, :string
  end
end
