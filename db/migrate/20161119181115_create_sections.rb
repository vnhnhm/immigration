class CreateSections < ActiveRecord::Migration[5.0]
  def change
    create_table :sections do |t|
      t.string :citation
      t.integer :section
      t.integer :level
      t.string :ina
      t.string :usc
      t.text :content

      t.timestamps
    end
  end
end
