# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
page = Page.create!
page.title = "home"
page.content = "Home page content"

puts "added page"
puts "starting sections"

require 'csv'

csv_text = File.read(Rails.root.join('lib','seeds','sections.csv'))
csv_text2 = File.read(Rails.root.join('lib','seeds','statutes.csv'))

csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO=8859-1')
csv.each do |row|
  p = Section.create!
  p.citation = row['citation']
  p.level = row['level']
  p.section = row['section']
  p.ina = row['ina']
  p.usc = row['usc']
  p.content = row['content']
  p.save
end

puts "sections complete"
puts "starting statutes"

csv2 = CSV.parse(csv_text2, :headers => true, :encoding => 'ISO=8859-1')
csv2.each do |row|
  p = Statute.create!
  p.ina = row['ina']
  p.usc = row['usc']
  p.title = row['title']
  p.save
end

puts "statutes complete"
puts "starting to create fullcite"

sec = Section.order(:id).all

lev = 0
inatracker = 101
cit = []

sec.each do |section|
  if section.level == 0
    section.fullcite = ""
  elsif section.level > lev
    lev += 1
    cit.push(section.citation) unless section.citation == ""
    section.fullcite = cit.map { |k| "(#{k})" }.join
  elsif section.level < lev
    cit.pop(lev - section.level + 1)
    lev -= (lev - section.level)
    cit.push(section.citation) unless section.citation == ""
    section.fullcite = cit.map { |k| "(#{k})" }.join
  elsif section.level == lev && section.citation.blank?
    if inatracker == section.ina
      section.fullcite = cit.map { |k| "(#{k})" }.join
    end
    if section.ina > inatracker
      cit.pop
      section.fullcite = cit.map { |k| "(#{k})" }.join
    end      
  else
    cit.pop
    cit.push(section.citation) unless section.citation == ""
    section.fullcite = cit.map { |k| "(#{k})" }.join
  end
  section.save
  inatracker = section.ina
  puts "saved #{section.fullcite}"
end