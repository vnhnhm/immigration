Rails.application.routes.draw do
  get 'users/new'

  get 'sessions/new'
  get 'sections/search'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "pages#show", :id => 4
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :pages, :users, :statutes, :resources, :sections

end
