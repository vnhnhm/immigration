json.extract! statute, :id, :ina, :usc, :title, :created_at, :updated_at
json.url statute_url(statute, format: :json)