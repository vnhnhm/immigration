json.extract! section, :id, :citation, :section, :level, :ina, :usc, :content, :created_at, :updated_at
json.url section_url(section, format: :json)