class Statute < ApplicationRecord
    has_many :sections, -> { order "id ASC" }, :foreign_key => "section"
    
    def to_param
        "#{id} #{title}".parameterize
    end
    
  def next
    Statute.where("id > ?", id).limit(1).first
  end

  def prev
    Statute.where("id < ?", id).last
  end
  
end
