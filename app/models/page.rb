class Page < ApplicationRecord
    def index
    end
    
    def home
    end
    
    def to_param
        "#{id} #{title}".parameterize
    end
end
