class Section < ApplicationRecord
    
    def to_param
        "#{id} INA #{ina} #{fullcite}".parameterize
    end
    
    def next
        self.class.where("id > ?", id).first
    end

    def previous
        self.class.where("id < ?", id).last
    end
    
    def self.search(search)
        where("content LIKE ?", "%#{search}%").order('id ASC')
    end
    
end
