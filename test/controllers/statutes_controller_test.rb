require 'test_helper'

class StatutesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @statute = statutes(:one)
  end

  test "should get index" do
    get statutes_url
    assert_response :success
  end

  test "should get new" do
    get new_statute_url
    assert_response :success
  end

  test "should create statute" do
    assert_difference('Statute.count') do
      post statutes_url, params: { statute: { ina: @statute.ina, title: @statute.title, usc: @statute.usc } }
    end

    assert_redirected_to statute_url(Statute.last)
  end

  test "should show statute" do
    get statute_url(@statute)
    assert_response :success
  end

  test "should get edit" do
    get edit_statute_url(@statute)
    assert_response :success
  end

  test "should update statute" do
    patch statute_url(@statute), params: { statute: { ina: @statute.ina, title: @statute.title, usc: @statute.usc } }
    assert_redirected_to statute_url(@statute)
  end

  test "should destroy statute" do
    assert_difference('Statute.count', -1) do
      delete statute_url(@statute)
    end

    assert_redirected_to statutes_url
  end
end
